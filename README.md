# Dynamic Time Warping (DTW)

Compare similarity of time series curve with 6 reference curves using distance calculated by DTW


## Getting Started

#### Clone the repo:
```
git clone git@bitbucket.org:blackswandatascience/dynamic_time_warping.git
```

#### Create a virtual environment

```bash
conda create --name dtw python=3.6
```

#### Install dependencies

**Dash**
```bash
pip install -r requirements.txt
```
