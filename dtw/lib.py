import numpy as np
from scipy import interpolate
from sklearn import preprocessing as prepo
import json
import matplotlib.pyplot as plt
import pandas as pd

scaler = prepo.MinMaxScaler()

def get_dtw(df_raw):
    """
    Main wrapper which takes only a time series dataframe
    :param df_raw:
    :return: pd.DataFrame with columns: Seasonal Faller, Seasonal Riser, Rising Star, Fallen Star, Sustain Riser, Sustain Faller
    """
    df_category = pd.DataFrame()
    for p in range(len(df_raw)):
        sublen = df_raw.iloc[p]
        x = range(len(df_raw.loc[sublen.name]))
        y = df_raw.loc[sublen.name].values

        # the num = 100 is hardcoded, it must be the same as the one inside the function reference_curves
        xvals = np.linspace(min(list(x)), max(list(x)), 100)
        yinterp = np.interp(xvals, x, y)
        ys_interp = scale_zero_one(xvals, yinterp)[:, 1]

        for i in bucket:
            DTW_Distance = DTWDistance(ys_interp, bucket[i])
            df_category.loc[sublen.name, i] = DTW_Distance
    return df_category  

def scale_zero_one(xvals, yinterp):
    """
    Helper function to scale x and y axis
    :param xvals:
    :param yinterp:
    :return:
    """
    scaled = scaler.fit(list(zip(xvals, yinterp)))
    scaled_xy = scaled.transform(list(zip(xvals, yinterp)))
    return scaled_xy

def DTWDistance(s1, s2):
    """
    Calculation using the series 1 and series 2.
    :param s1: single array of time series value of item
    :param s2: single array of time series value of a reference curve in the bucket
    :return: float value to be populated into a dataframe
    """
    DTW={}

    #TODO: convert this process to a vectorization approach
    for i in range(len(s1)):
        DTW[(i, -1)] = float('inf')
    for i in range(len(s2)):
        DTW[(-1, i)] = float('inf')
    DTW[(-1, -1)] = 0
    
    for i in range(len(s1)):
        for j in range(len(s2)):
            dist= (s1[i]-s2[j])**2 
            DTW[(i, j)] = dist + min(DTW[(i-1, j)],DTW[(i, j-1)], DTW[(i-1, j-1)])

    return np.sqrt(DTW[len(s1)-1, len(s2)-1])

def plot_all_7_curves_with_sublens(df_raw, item):
    """
    Visualisation helper to plot time series of item against each reference curve on a subplot
    :param df_raw:
    :param item:
    :return:
    """
    for p in [item]:
        sublen = df_raw.loc[p, :]
        x = range(len(df_raw.loc[sublen.name]))
        y = df_raw.loc[sublen.name].values

        xvals = np.linspace(min(list(x)), max(list(x)), num)
        yinterp = np.interp(xvals, x, y)

        ys_interp = scale_zero_one(xvals, yinterp)[:, 1]

        n = 1
        for i in bucket:
            DTW_Distance = DTWDistance(ys_interp, bucket[i])
            plot_superimpose(xvals, ys_interp, i, DTW_Distance, bucket, n)
            n +=1
        plt.suptitle(sublen.name, size = 20)
        plt.show()

def plot_superimpose(xvals, ys_interp, i, DTW_Distance, bucket, n):
    """
    Superimposes series1 and series2
    :param xvals:
    :param ys_interp:
    :param i:
    :param DTW_Distance:
    :param bucket: a bucket contains all the reference curves
    :param n:
    :return:
    """
    plt.figure(1, figsize=(10, 8))
    plt.subplot(3, 3, n)
    plt.plot(xvals, ys_interp, label = 'Original')
    plt.plot(xvals, bucket[i], alpha = 0.7, label = 'Fit')
    plt.legend()
    plt.title('{}: {}'.format(i, round(DTW_Distance, 3)))

def get_metrics(df_category):
    df_category_analysis = pd.DataFrame()
    df_category_analysis['category'] = df_category.idxmin(axis=1)
    df_category_analysis['std'] = np.std(df_category, axis = 1)
    return df_category_analysis


def display_buckets():
    """
    To be called to view reference curves in the bucket
    :return:
    """
    n =1
    for i in bucket:
        plt.subplot(3, 3, n)
        plt.plot(bucket[i])
        n +=1
        plt.xticks([])
        plt.yticks([])
        plt.title(i)
    plt.tight_layout()

"""
Define the reference curves here:
6 reference curves, to be evaluated.
"""
num = 100
c = 0
m = 1
n = int(0.7 * num)

x1 = np.linspace(0, 26, num=num)

y_sustainedRiser = np.exp(x1*0.1)
y_sustainedFaller = 1/(x1+1) # +1, cannot divide by 0
y_seasonalRisers = x1*np.sin(x1/2)**2 + 0.5*x1
y_seasonalFallers = np.sin(x1/2)**2/(x1+1) - x1/200 #+1, cannot divide by 0
y_risingStar = np.exp(x1)
y_fallenStar = [np.exp(i*0.5) for i in x1[:n]] + [1/i for i in x1[n:]]
y_seasonal_Nathan= (np.sin(x1/4.2)**2)


bucket = {'Sustained Riser': scale_zero_one(x1, y_sustainedRiser)[:, 1], 
          'Sustained Faller': scale_zero_one(x1, y_sustainedFaller)[:, 1],
         'Rising Star': scale_zero_one(x1, y_risingStar)[:, 1], 
         'Fallen Star': scale_zero_one(x1, y_fallenStar)[:, 1],
         'Seasonal Riser': scale_zero_one(x1, y_seasonalRisers)[:, 1], 
         'Seasonal Faller': scale_zero_one(x1, y_seasonalFallers)[:, 1],
         'Seasonal Nathan': scale_zero_one(x1, y_seasonal_Nathan)[:, 1]}