#!/usr/bin/env python
from distutils.core import setup

setup(name='dynamic_time_warping',
      version='0.0.1',
      description='Dynamic Time Wapring Curve Classification',
      install_requires=[open('requirements.txt').read().splitlines()],
      packages=find_packages(),
      author='Winson Lam',
      author_email='winson.lam@blackswan.com')
